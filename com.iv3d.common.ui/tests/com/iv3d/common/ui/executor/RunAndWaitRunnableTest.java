/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.ui.executor;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.iv3d.common.ui.Callback;
import com.iv3d.common.ui.UiTask;
import com.iv3d.common.ui.UiTaskResult;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.VerificationsInOrder;

public class RunAndWaitRunnableTest extends TestCase {
	
	@Mocked
	private Callback<UiTaskResult> doneCallback;
	@Mocked
	private UiTaskResult uiResult;
	@Mocked
	private Future<UiTaskResult> future;
	
	public void testRun() throws Exception {
		final long timeout = 1234;
		final TimeUnit unit = TimeUnit.MILLISECONDS;
		
		new NonStrictExpectations() {

			{	
				future.get(timeout, unit);
				result = uiResult;
			}
		};
		
		RunAndWaitRunnable<UiTask> runnable = new RunAndWaitRunnable<UiTask>(timeout, future, doneCallback);
		runnable.run();
		
		new VerificationsInOrder() {
			{	
				doneCallback.invoke(uiResult);
				times=1;
			}
		};
	}
}
