/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.ui;

import com.iv3d.common.Percentage;
import com.iv3d.common.ui.UiTaskStatus;
import com.iv3d.common.ui.UiTaskStatusBuilder;

import junit.framework.TestCase;

public class UiTaskStatusTest extends TestCase {
	
	public void testMessage() {
		UiTaskStatus instance = new UiTaskStatusBuilder()
			.setMessage("hello")
			.build();
		
		assertNotNull(instance);
		assertEquals("hello", instance.getMessage());
		assertEquals(0, instance.getPercentage().get());
	}
	
	public void testProgress() {
		UiTaskStatus instance = new UiTaskStatusBuilder()
			.setPercentage(new Percentage(20))
			.build();
		
		assertNotNull(instance);
		assertEquals("", instance.getMessage());
		assertEquals(20, instance.getPercentage().get());
	}
	
	public void testProgressMessage() {
		UiTaskStatus instance = new UiTaskStatusBuilder()
			.setMessage("busy")
			.setPercentage(new Percentage(30))
			.build();
		
		assertNotNull(instance);
		assertEquals("busy", instance.getMessage());
		assertEquals(30, instance.getPercentage().get());
	}
}
