/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.ui;

import com.iv3d.common.Percentage;

/**
 * A builder for task status values.
 */
public final class UiTaskStatusBuilder {

	private String message;
	private Percentage percentage;
	
	/**
	 * Default constructor.
	 */
	public UiTaskStatusBuilder() {
		message = "";
		percentage = new Percentage();
	}
	
	/**
	 * @param message The status message.
	 * @return The {@link UiTaskResultBuilder}
	 */
	public final UiTaskStatusBuilder setMessage(String message) {
		this.message = message;
		return this;
	}
	
	/**
	 * @param percentage The status percentage values.
	 * @return The {@link UiTaskResultBuilder}
	 */
	public final UiTaskStatusBuilder setPercentage(Percentage percentage) {
		this.percentage = percentage;
		return this;
	}
	
	/**
	 * Build a new {@link UiTaskStatus} object.
	 * 
	 * @return The new {@link UiTaskStatus}.
	 */
	public final UiTaskStatus build() { 
		return new Instance(this);
	}
	
	private class Instance implements UiTaskStatus {

		private final String message;
		private final Percentage percentage;
	
		private Instance(UiTaskStatusBuilder builder) {
			this.message = builder.message;
			this.percentage = builder.percentage;
		}
	
		public final String getMessage() {
			return message;
		}
	
		public final Percentage getPercentage() {
			return percentage;
		}
	}
	
}
