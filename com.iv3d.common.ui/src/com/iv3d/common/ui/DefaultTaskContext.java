package com.iv3d.common.ui;

import java.util.HashMap;
import java.util.Map;

public class DefaultTaskContext implements UiTaskContext, UiCancelable {
	private boolean canceled;
	private Callback<UiTaskStatus> updateCallable;
	private final Map<String, Object> params;
	
	public DefaultTaskContext() {
		this.params = new HashMap<String, Object>();
	}

	@Override
	public final void postUpdate(UiTaskStatus status) {
		if(this.updateCallable!=null) {
			this.updateCallable.invoke(status);
		}
	}
	
	@Override
	public final boolean isCanceled() {
		return this.canceled;
	}

	@Override
	public final void cancel() {
		this.canceled = true;
	}
	
	@Override
	public final <T> T getParam(String paramName, Class<T> type) {
		return this.params.containsKey(paramName)?type.cast(params.get(paramName)):null;
	}

	public final void addParam(String key, Object value) {
		this.params.put(key, value);
	}

	public final void setTaskUpdateCallback(Callback<UiTaskStatus> updateCallable) {
		this.updateCallable = updateCallable;
	}
}
