/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.ui.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import com.iv3d.common.ui.Callback;
import com.iv3d.common.ui.UiTask;
import com.iv3d.common.ui.UiTaskBootstrap;
import com.iv3d.common.ui.UiTaskContext;
import com.iv3d.common.ui.UiTaskResult;

/**
 * Executor used to run tasks no an {@link ExecutorService}.
 */
public final class UiTaskExecuter<TaskType extends UiTask> {
	private final ExecutorService executorService;
	private final UiTaskContext taskContext;
	
	private Callback<UiTaskResult> doneCallback;
	private long timeout=0L;
	private UiTaskBootstrap<TaskType> taskBootstrap;
	
	/**
	 * Default constructor.
	 * 
	 * @param executorService The executor service.
	 * @param doneCallback The {@link Callback} invoked when the task is done.
	 * @param taskContext The task context in which the task is performed.
	 * @param timeout The task timeout.
	 */
	public UiTaskExecuter(ExecutorService executorService,
			UiTaskContext taskContext){
		this.executorService = executorService;
		this.taskContext = taskContext;
	}
	
	public final UiTaskExecuter<TaskType> setDoneCallback(Callback<UiTaskResult> doneCallback) {
		this.doneCallback = doneCallback;
		return this;
	}

	public final UiTaskExecuter<TaskType> setTimeout(long timeout) {
		this.timeout = timeout<=0?0:timeout;
		return this;
	}

	public UiTaskExecuter<TaskType> setTaskBootstrap(UiTaskBootstrap<TaskType> taskBootstrap) {
		this.taskBootstrap = taskBootstrap;
		return this;
	}

	/**
	 * Run the given task.
	 * @param task The task to be executed.
	 */
	public final void execute(final TaskType task) {
		UiTaskCallableAdapter<TaskType> callable = new UiTaskCallableAdapter<TaskType>(task, taskContext, taskBootstrap);
		Future<UiTaskResult> future = executorService.submit(callable);
		
		RunAndWaitRunnable<TaskType> command = new RunAndWaitRunnable<TaskType>(timeout, future, doneCallback);
		executorService.execute(command);
	}
	

}
