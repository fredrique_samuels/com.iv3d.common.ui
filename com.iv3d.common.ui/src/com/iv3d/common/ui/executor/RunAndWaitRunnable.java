/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.ui.executor;

import java.util.concurrent.Future;

import com.iv3d.common.ui.Callback;
import com.iv3d.common.ui.UiTask;
import com.iv3d.common.ui.UiTaskResult;

class RunAndWaitRunnable<TaskType extends UiTask> implements Runnable {
		
		private long timeout;
		private Callback<UiTaskResult> doneCallback;
		private Future<UiTaskResult> callableFuture;

		public RunAndWaitRunnable(long timeout, Future<UiTaskResult> callableFuture,
				Callback<UiTaskResult> doneCallback) {
			this.timeout = timeout;
			this.callableFuture = callableFuture;
			this.doneCallback = doneCallback;
		}

		@Override
		public void run() {
			TaskResultProvider taskFuture = new FutureTaskResultProviderBuilder()
				.setFuture(callableFuture)
				.setTimeout(timeout)
				.build();
			UiTaskResult result = taskFuture.getResult();
			doneCallback.invoke(result);
		}
	}