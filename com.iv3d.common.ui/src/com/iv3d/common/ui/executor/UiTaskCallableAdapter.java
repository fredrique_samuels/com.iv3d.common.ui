/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common.ui.executor;

import java.util.concurrent.Callable;

import com.iv3d.common.ui.UiTask;
import com.iv3d.common.ui.UiTaskBootstrap;
import com.iv3d.common.ui.UiTaskContext;
import com.iv3d.common.ui.UiTaskResult;

class UiTaskCallableAdapter<TaskType extends UiTask> implements Callable<UiTaskResult> {
	private final TaskType task;
	private final UiTaskContext taskContext;
	private UiTaskBootstrap<TaskType> bootstrap;

	public UiTaskCallableAdapter(TaskType task,
			UiTaskContext taskContext,
			UiTaskBootstrap<TaskType> bootstrap) {
		this.task = task;
		this.taskContext = taskContext;
		this.bootstrap = bootstrap;
	}

	@Override
	public UiTaskResult call() throws Exception {
		if(bootstrap!=null) {bootstrap.bootstrap(task);}
		return task.run(taskContext);
	}

}